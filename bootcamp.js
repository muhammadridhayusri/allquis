const fs = require('fs')
const path = require('path')
const data = [{
   name : "budi",
   address : "indo"
}]

const filename = "./test/tex1.txt"
// fs.writeFileSync(filename,JSON.stringify(data),'utf-8')
// const datatxt = fs.readFileSync('./test/tex1.txt','utf-8')
// console.log(JSON.parse(datatxt))

class FileManager {
   constructor(filename) {
      this.filename = filename
      this.data = this.readFile(this.filename)
   }

   readFile(path) {
      const data = JSON.parse(fs.readFileSync(path,'utf-8'))
      console.log(data)
      return data
   }

   showdata() {
      let y = this.data
      console.log(y)
   }

   addData(newData) {
      let dataOlahan = this.data
      dataOlahan.push(newData)
      const finaldata= JSON.stringify(dataOlahan)
      fs.writeFileSync(this.filename, finaldata)
      console.log(dataOlahan)
   }

   deleteData(name) {
      let _finaldata = this.data
      let idx = _finaldata.findIndex(x => x.name === name)
      _finaldata.splice(idx,1)
      fs.writeFileSync(this.filename, JSON.stringify(_finaldata))
   }

   changeData(before, after) {
      let _finaldata = this.data
      let idx =_finaldata.findIndex(x => x.name === before)
      _finaldata[idx].name = after
      fs.writeFileSync(this.filename,JSON.stringify(_finaldata))
   }
}

const data1 = {
   name: "adi",
   addres: "jakarta"
}

const data2 = {
   name: "bambang",
   addres: "bandung"
}

let x = new FileManager(filename)
// x.showdata()
// x.addData(data1)
// x.addData(data2)
x.deleteData("putra")
// x.changeData("bambang","putra")
// FileManager.addData(data1)
